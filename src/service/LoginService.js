import api from './api'

class LoginService{

     login(obj){
          return api.post('/login', obj);
     }

}

export default new LoginService();