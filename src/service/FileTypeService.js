import api from './api'

class FileTypeService {
    getAll() {
        return api.get('/tipo-archivos');
    }

    getById(id) {
        return api.get(`/tipo-archivos/${id}`);
    }

    create(obj) {
        return api.post('/tipo-archivos', obj);
    }

    update(obj) {
        return api.put('/tipo-archivos', obj);
    }

    delete(id) {
        return api.delete(`/tipo-archivos/${id}`)
    }
}

export default new FileTypeService();