import api from "./api";

class DocumentService {
    getAll() {
        return api.get("/documentos");
    }

    getById(id) {
        return api.get(`/documentos/${id}`);
    }

    create(obj) {
        return api.post("/documentos", obj);
    }

    update(obj) {
        return api.put("/documentos", obj);
    }

    delete(id) {
        return api.delete(`/documentos/${id}`);
    }
}

export default new DocumentService();
