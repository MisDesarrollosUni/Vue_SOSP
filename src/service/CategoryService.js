import api from './api'

/* 
    ? Retorna http://localhost:8080/api, y con esta clase llega a concatenarse dependiendo
    ? el método
*/

class CategoryService {
    getAll() {
        return api.get('/categorias');
    }

    getById(id) {
        return api.get(`/categorias/${id}`);
    }

    create(obj) {
        return api.post('/categorias', obj);
    }

    update(obj) {
        return api.put('/categorias', obj);
    }

    delete(id) {
        return api.delete(`/categorias/${id}`)
    }

    getAllTrues() {
        return api.get('/categorias/trues');
    }

}

export default new CategoryService();