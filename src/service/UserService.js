import api from './api'

class UserService{

     getAll(){
          return api.get('/users');
     }

     create(obj){
          return api.post('/users', obj);
     }
}

export default new UserService();