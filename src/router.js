import Vue from "vue";
import Router from "vue-router";

Vue.use(Router);

export default new Router({
    /*
     * history - usa la parte normal
     * has - coloca despues de localhost una almohadilla //#endregion
     */
    mode: "history",
    routes: [
        // !Cuando el usuario entra a entra la ruta principal con / osea -> http://localhost:8081:/

        {
            path: "/", // ?en el local lo primero que muestra seria el componentes ListCategory
            name: "main", //?porque lo estamos importando
            component: () => import("./components/login/Login"),
            // import ('./components/document/InboxDocument')
        },
        {
            path: "/dashboard", // ?en el local lo primero que muestra seria el componentes ListCategory
            name: "main", //?porque lo estamos importando
            component: () => import("./components/document/InboxDocument"),
        },
        {
            path: "/categories", // ?en el local lo primero que muestra seria el componentes ListCategory
            name: "categorias", //?porque lo estamos importando
            component: () => import("./components/category/ListCategory"),
        },
        {
            path: "/addCategory",
            name: "addCategory", // ? tambien se puede así, importanto desde arriba
            component: () => import("./components/category/AddCategory"),
        },
        {
            path: "/categorias/:id", // ? Espera un id
            name: "editCategory",
            component: () => import("./components/category/EditCategory"),
        },
        {
            path: "/listFileType",
            name: "listFileType",
            component: () => import("./components/filetype/ListFileType"),
        },
        {
            path: "/addFileType",
            name: "addFileType",
            component: () => import("./components/filetype/AddFileType"),
        },
        {
            path: "/editFileType/:id",
            name: "editFileType",
            component: () => import("./components/filetype/EditFileType"),
        },
    ],
});
