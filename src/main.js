 // *Aqui irian algunas importaciones como por ejemplo bootsrap
 import Vue from 'vue'
 import App from './App.vue'
 import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'; // ? importación del modulo y importación de iconos
 import router from './router'

 Vue.config.productionTip = false

 // ? Importar las hojas de estilo boostrap 
 import 'bootstrap/dist/css/bootstrap.css'
 import 'bootstrap-vue/dist/bootstrap-vue.css'

 // ? Y decirle a vue que las vamos a usar 
 Vue.use(BootstrapVue)
 Vue.use(IconsPlugin)

 new Vue({
     router: router,
     render: h => h(App),
 }).$mount('#app')